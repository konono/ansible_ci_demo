from ansiblelint.constants import odict
from ansiblelint.errors import MatchError
from ansiblelint.file_utils import Lintable
from ansiblelint.rules import AnsibleLintRule
from typing import Any
from typing import List


class YamlExtensionRule(AnsibleLintRule):
    id = 'yaml-extension'
    shortdesc = 'Playbooks should have the ".yml" extension'
    description = shortdesc
    tags = ['yaml']
    done = []

    def matchplay(
        self, file: Lintable, data: "odict[str, Any]"
    ) -> List[MatchError]:
        if file.path.suffix in ('.yaml'):
            return [
                self.create_matcherror(
                    message="Playbook doesn't have '.yml' extension: " +
                    str(data['__file__']) + ". " + self.shortdesc,
                    linenumber=data['__line__'],
                    filename=file
                )
            ]
        return []
